import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");


describe("Bonus tests > ", () => {
    console.log("Tests started");

    test("Standard program with small value",  (done) => {
        const amount = Math.random() * 10000;
        assert.equal(calculateBonuses("Standard", amount), 0.05);
        done();
    });

    test("Standard program with boundary value",  (done) => {
        const amount = 10000;
        assert.equal(calculateBonuses("Standard", amount), 1.5 * 0.05);
        done();
    });

    test("Standard program with medium value",  (done) => {
        const amount = 10000 + Math.random() * 40000;
        assert.equal(calculateBonuses("Standard", amount), 1.5 * 0.05);
        done();
    });

    test("Standard program with boundary value",  (done) => {
        const amount = 50000;
        assert.equal(calculateBonuses("Standard", amount), 2 * 0.05);
        done();
    });

    test("Standard program with large value",  (done) => {
        const amount = 50000 + Math.random() * 50000;
        assert.equal(calculateBonuses("Standard", amount), 2 * 0.05);
        done();
    });

    test("Standard program with boundary value",  (done) => {
        const amount = 100000;
        assert.equal(calculateBonuses("Standard", amount), 2.5 * 0.05);
        done();
    });

    test("Standard program with extra large value",  (done) => {
        const amount = 100000 + Math.random() * 100;
        assert.equal(calculateBonuses("Standard", amount), 2.5 * 0.05);
        done();
    });

    test("Premium program with small value",  (done) => {
        const amount = Math.random() * 10000;
        assert.equal(calculateBonuses("Premium", amount), 0.1);
        done();
    });

    test("Premium program with boundary value",  (done) => {
        const amount = 10000;
        assert.equal(calculateBonuses("Premium", amount), 1.5 * 0.1);
        done();
    });

    test("Premium program with medium value",  (done) => {
        const amount = 10000 + Math.random() * 40000;
        assert.equal(calculateBonuses("Premium", amount), 1.5 * 0.1);
        done();
    });

    test("Premium program with boundary value",  (done) => {
        const amount = 50000;
        assert.equal(calculateBonuses("Premium", amount), 2 * 0.1);
        done();
    });

    test("Premium program with large value",  (done) => {
        const amount = 50000 + Math.random() * 50000;
        assert.equal(calculateBonuses("Premium", amount), 2 * 0.1);
        done();
    });

    test("Premium program with boundary value",  (done) => {
        const amount = 100000;
        assert.equal(calculateBonuses("Premium", amount), 2.5 * 0.1);
        done();
    });

    test("Premium program with extra large value",  (done) => {
        const amount = 100000 + Math.random() * 100;
        assert.equal(calculateBonuses("Premium", amount), 2.5 * 0.1);
        done();
    });

    test("Diamond program with small value",  (done) => {
        const amount = Math.random() * 10000;
        assert.equal(calculateBonuses("Diamond", amount), 0.2);
        done();
    });

    test("Diamond program with boundary value",  (done) => {
        const amount = 10000;
        assert.equal(calculateBonuses("Diamond", amount), 1.5 * 0.2);
        done();
    });

    test("Diamond program with medium value",  (done) => {
        const amount = 10000 + Math.random() * 40000;
        assert.equal(calculateBonuses("Diamond", amount), 1.5 * 0.2);
        done();
    });

    test("Diamond program with boundary value",  (done) => {
        const amount = 50000;
        assert.equal(calculateBonuses("Diamond", amount), 2 * 0.2);
        done();
    });

    test("Diamond program with large value",  (done) => {
        const amount = 50000 + Math.random() * 50000;
        assert.equal(calculateBonuses("Diamond", amount), 2 * 0.2);
        done();
    });

    test("Diamond program with boundary value",  (done) => {
        const amount = 100000;
        assert.equal(calculateBonuses("Diamond", amount), 2.5 * 0.2);
        done();
    });

    test("Diamond program with extra large value",  (done) => {
        const amount = 100000 + Math.random() * 100;
        assert.equal(calculateBonuses("Diamond", amount), 2.5 * 0.2);
        done();
    });

    test("ArbitRarrY program with small value",  (done) => {
        const amount = Math.random() * 10000;
        assert.equal(calculateBonuses("ArbitRarrY", amount), 0);
        done();
    });

    test("ArbitRarrY program with boundary value",  (done) => {
        const amount = 10000;
        assert.equal(calculateBonuses("ArbitRarrY", amount), 0);
        done();
    });

    test("ArbitRarrY program with medium value",  (done) => {
        const amount = 10000 + Math.random() * 40000;
        assert.equal(calculateBonuses("ArbitRarrY", amount), 0);
        done();
    });

    test("ArbitRarrY program with boundary value",  (done) => {
        const amount = 50000;
        assert.equal(calculateBonuses("ArbitRarrY", amount), 0);
        done();
    });

    test("ArbitRarrY program with large value",  (done) => {
        const amount = 50000 + Math.random() * 50000;
        assert.equal(calculateBonuses("ArbitRarrY", amount), 0);
        done();
    });

    test("ArbitRarrY program with boundary value",  (done) => {
        const amount = 100000;
        assert.equal(calculateBonuses("ArbitRarrY", amount), 0);
        done();
    });

    test("ArbitRarrY program with extra large value",  (done) => {
        const amount = 100000 + Math.random() * 100;
        assert.equal(calculateBonuses("ArbitRarrY", amount), 0);
        done();
    });

    console.log("Tests Finished");
});
